<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
    </ol>
    <div class="carousel-inner">
        <div class="item active">
            <div class="container">
                <div class="carousel-caption">
                    <h1>Encrypt</h1>
                    <p>Securely encrypted data is completely protected, even if it is stolen. Why? If, for example, a file is encrypted with 256-bit AES, it would take a hacker more than a lifetime to crack the code using the brute-force method.</p>
                    <p><a class="btn btn-lg btn-primary" href="<c:url value='<%= MainController.RQ_ENCRYPT_PAGE%>'/>">Encrypt</a></p>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="container">
                <div class="carousel-caption">
                    <h1>Decrypt</h1>
                    <p>To reveal the content of an encrypted file you need to decrypt them first</p>
                    <p><a class="btn btn-lg btn-primary" href="<c:url value='<%= MainController.RQ_DECRYPT_PAGE%>'/>">Decrypt</a></p>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="container">
                <div class="carousel-caption">
                    <h1>Key Management</h1>
                    <p>Key management is the management of cryptographic keys in a cryptosystem. This includes dealing with the generation, exchange, storage, use, and replacement of keys.</p>
                    <p><a class="btn btn-lg btn-primary" href="<c:url value='<%= MainController.RQ_KEY_MANAGEMENTS_PAGE%>'/>">Decrypt</a></p>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="container">
                <div class="carousel-caption">
                    <h1>Decrypt</h1>
                    <p>To reveal the content of an encrypted file you need to decrypt them first</p>
                    <p><a class="btn btn-lg btn-primary" href="<c:url value='<%= MainController.RQ_DECRYPT_PAGE%>'/>">Decrypt</a></p>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="container">
                <h1>Decrypt</h1>
                <p>To reveal the content of an encrypted file you need to decrypt them first</p>
                <p><a class="btn btn-lg btn-primary" href="<c:url value='<%= MainController.RQ_DECRYPT_PAGE%>'/>">Decrypt</a></p>
            </div>
        </div>
    </div>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>