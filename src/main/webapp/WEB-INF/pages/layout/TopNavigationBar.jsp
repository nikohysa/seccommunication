<%-- 
    Document   : TopNavigationBar
    Created on : Jul 14, 2016, 2:52:04 PM
    Author     : Niko
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="net.SeniorProject.controller.MainController" %>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"/>
                <span class="icon-bar"/>
                <span class="icon-bar"/>
            </button>
            <a class="navbar-brand" href="<c:url value='<%= MainController.RQ_INDEX%>'/>">NH-Senior Project</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a href="<c:url value='<%= MainController.RQ_ENCRYPT_PAGE%>'/>">Encrypt</a>
                </li>
                <li>
                    <a href="<c:url value='<%= MainController.RQ_DECRYPT_PAGE%>'/>">Decrypt</a>
                </li>
                <li>
                    <a href="<c:url value='<%= MainController.RQ_KEY_MANAGEMENTS_PAGE%>'/>">Key managements</a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="<c:url value='<%= MainController.RQ_SIGN_PAGE %>'/>">Sign</a>
                </li>
                <li>
                    <a href="<c:url value='<%= MainController.RQ_VERIFY_PAGE %>'/>">Verify</a>
                </li>
            </ul>
        </div>
    </div>
</nav>