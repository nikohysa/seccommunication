package net.SeniorProject.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainController {

    public static final String RQ_INDEX = "/";
    public static final String RQ_ENCRYPT_PAGE = "/encrypt";
    public static final String RQ_DECRYPT_PAGE = "/decrypt";
    public static final String RQ_KEY_MANAGEMENTS_PAGE = "/keymanagement";
    public static final String RQ_SIGN_PAGE = "/sign";
    public static final String RQ_VERIFY_PAGE = "/verify";

    public static final String VW_INDEX = "/index";
    public static final String VW_ENCRYPT_PAGE = "/encrypt";
    public static final String VW_DECRYPT_PAGE = "/decrypt";
    public static final String VW_KEY_MANAGEMENTS_PAGE = "/keymanagement";
    public static final String VW_SIGN_PAGE = "/sign";
    public static final String VW_VERIFY_PAGE = "/verify";

    public static final String PR_PAGE_TITILE = "title";

    @RequestMapping(value = {RQ_INDEX})
    public ModelAndView getIndex() {
        ModelAndView mav = new ModelAndView(VW_INDEX);
        mav.addObject(PR_PAGE_TITILE, "Secure Communication");
        return mav;
    }

    @RequestMapping(value = RQ_ENCRYPT_PAGE)
    public ModelAndView getEncryptPage() {
        ModelAndView modelAndView = new ModelAndView(VW_ENCRYPT_PAGE);
        modelAndView.addObject(PR_PAGE_TITILE, "Secure Communication - ENCRYPT");
        return modelAndView;
    }

    @RequestMapping(value = RQ_DECRYPT_PAGE)
    public ModelAndView getDecryptionPage() {
        ModelAndView modelAndView = new ModelAndView(VW_DECRYPT_PAGE);
        modelAndView.addObject(PR_PAGE_TITILE, "Secure Communication - DECRYPT");
        return modelAndView;
    }

    @RequestMapping(value = RQ_KEY_MANAGEMENTS_PAGE)
    public ModelAndView getKeyManagementPage() {
        ModelAndView modelAndView = new ModelAndView(VW_KEY_MANAGEMENTS_PAGE);
        modelAndView.addObject(PR_PAGE_TITILE, "Secure Communication - Key Management");
        return modelAndView;
    }

    @RequestMapping(value = RQ_SIGN_PAGE)
    public ModelAndView getSignPage() {
        ModelAndView modelAndView = new ModelAndView(VW_SIGN_PAGE);
        modelAndView.addObject(PR_PAGE_TITILE, "Secure Communication - Sign");
        return modelAndView;
    }

    @RequestMapping(value = RQ_VERIFY_PAGE)
    public ModelAndView getVerifyPage() {
        ModelAndView modelAndView = new ModelAndView(VW_VERIFY_PAGE);
        modelAndView.addObject(PR_PAGE_TITILE, "Secure Communication - Verify");
        return modelAndView;
    }
}
